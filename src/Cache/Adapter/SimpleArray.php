<?php
namespace Sl\Cache\Adapter;

use Sl\Cache\AdapterInterface;

class SimpleArray implements AdapterInterface
{
    private $cached = array();
    
    public function clean()
    {
        $this->cached = array();
    }

    public function load($cacheId)
    {
        if(!$this->test($cacheId)) {
            return null;
        }
        return $this->cached[$cacheId];
    }

    public function save($data, $cacheId)
    {
        $this->cached[$cacheId] = $data;
    }

    public function test($cacheId)
    {
        return isset($this->cached[$cacheId]);
    }
}