<?php
namespace Sl\Helper\Config\Writer;

use Sl\Helper\Config\WriterInterface;

class ArrayWriter implements WriterInterface
{
    public function write($filePath, array $config = array())
    {
        if(!file_exists($filePath)) {
            touch($filePath);
        }

        return (bool) file_put_contents($filePath, $this->getContent($config));
    }

    protected function getContent(array $config, $level = 0)
    {
        $content = '';
        $openAndCloseNeeded = false;

        if($level === 0) {
            $openAndCloseNeeded = true;
            $content = '<?php return array('.PHP_EOL;
        }
        $level++;
        foreach($config as $key=>$value) {
            if(is_array($value)) {
                $valueContent = 'array('.PHP_EOL.$this->getContent($value, $level).str_repeat(' ', $level*4).')';
            } else {
                $valueContent = '\''.$value.'\'';
            }
            $content .= str_repeat(' ', $level*4).'\''.$key.'\' => '.$valueContent.','.PHP_EOL;
        }

        if($openAndCloseNeeded) {
            $content .= ');';
        }

        return $content;
    }
}