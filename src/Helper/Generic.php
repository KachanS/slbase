<?php
namespace Sl\Helper;

class Generic
{
    public static function merge(array $base, array $extender)
    {
        foreach($extender as $k=>$i) {
            if(isset($base[$k]) && is_array($base[$k]) && is_array($i)) {
                $base[$k] = self::merge($base[$k], $i);
            } else {
                $base[$k] = $i;
            }
        }
        return $base;
    }

    /**
     *
     * @param array $data
     * @param string $key
     * @param mixed $default
     * @return mixed
     */
    public static function arrayValue(array $data, $key, $default = null, $pathSeparator = '.')
    {
        $subKeys = explode($pathSeparator, $key);
        
        if(count($subKeys) === 1) {
            return isset($data[$key])?$data[$key]:$default;
        } else {
            $currentSubKey = array_shift($subKeys);
            if(isset($data[$currentSubKey]) && is_array($data[$currentSubKey])) {
                return self::arrayValue($data[$currentSubKey], implode($pathSeparator, $subKeys), $default, $pathSeparator);
            }
            return $default;
        }
    }
}