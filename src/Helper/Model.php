<?php
namespace Sl\Helper;

class Model
{
    const ALIAS_SEPARATOR = '.';

    const METHOD_PREFIX_SET = 'set';
    const METHOD_PREFIX_GET = 'get';

    public static function getAlias(\Sl\Model\ModelInterface $model)
    {
        return implode(self::ALIAS_SEPARATOR, array_filter(array($model->findModuleName(), $model->findModelName())));
    }

    public static function buildSetter($property)
    {
        return self::buildMethod($property, self::METHOD_PREFIX_SET);
    }

    public static function buildGetter($property)
    {
        return self::buildMethod($property, self::METHOD_PREFIX_GET);
    }

    public static function buildMethod($property, $prefix)
    {
        return $prefix.implode('', array_map('ucfirst', array_map('strtolower', explode('_', $property))));
    }
}