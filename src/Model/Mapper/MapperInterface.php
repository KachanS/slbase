<?php
namespace Sl\Model\Mapper;

use Sl\Model\ModelInterface;

interface MapperInterface
{
    public function countRelated(ModelInterface $model, $relation);
    public function createNew();

    public function create(ModelInterface $model);
    public function delete(ModelInterface $model);
    public function duplicate(ModelInterface $model);
    public function validate(ModelInterface $model);

    public function find($id);
    public function fetchAll();
}