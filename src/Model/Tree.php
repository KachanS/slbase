<?php
namespace Sl\Model;

abstract class Tree extends Basic implements TreeModelInterface
{
    protected $left_key;
    protected $right_key;
    protected $level;
    
    public function getLeftKey()
    {
        return $this->left_key;
    }

    public function getLevel()
    {
        return $this->level;
    }

    public function getRightKey()
    {
        return $this->right_key;
    }

    public function setLeftKey($leftKey)
    {
        $this->left_key = $leftKey;
        return $this;
    }

    public function setLevel($level)
    {
        $this->level = $level;
        return $this;
    }

    public function setRightKey($rightKey)
    {
        $this->right_key = $rightKey;
        return $this;
    }
}