<?php
namespace Sl\Model;

interface TreeModelInterface extends ModelInterface
{
    public function setLeftKey($leftKey);
    public function setRightKey($rightKey);
    public function setLevel($level);

    public function getLeftKey();
    public function getRightKey();
    public function getLevel();
}