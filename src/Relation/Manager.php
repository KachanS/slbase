<?php
namespace Sl\Relation;

class Manager
{
    const ESSENTIAL_OPTION_KEY = 'essentials';
    const RELATION_FIELD_PREFIX = 'modulerelation';
}