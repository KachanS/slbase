<?php
namespace Sl\Relation;

interface RelationInterface
{
    public function getName();
    public function getType();
    
    public function hasOption($name);
    public function getOption($name);
    public function getOptions();
    
    public function getTable();
    public function getRelatedTable();
    public function getIntersectionTable();
}