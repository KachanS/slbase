<?php
namespace Sl\Cache\Adapter;

class SimpleArrayTest extends \PHPUnit_Framework_TestCase
{
    /**
     *
     * @var SimpleArray
     */
    protected $adapter;
    
    public function setUp()
    {
        $this->adapter = new SimpleArray();
    }

    public function testInitial()
    {
        $this->assertAttributeEmpty('cached', $this->adapter);
    }

    /**
     * @dataProvider saveData
     */
    public function testSave($data)
    {
        $this->adapter->save($data, 'cacheId');
        $this->assertAttributeSame(array('cacheId' => $data), 'cached', $this->adapter);
    }

    public function testTest()
    {
        $this->adapter->save('data1', 'cache1');

        $this->assertFalse($this->adapter->test('cache2'));
        $this->assertTrue($this->adapter->test('cache1'));
    }

    public function testLoad()
    {
        $object = new \stdClass();
        $this->adapter->save('data1', 'cache1');
        $this->adapter->save($object, 'cache2');

        $this->assertNull($this->adapter->load('cache3'));
        $this->assertSame('data1', $this->adapter->load('cache1'));
        $this->assertSame($object, $this->adapter->load('cache2'));
    }

    public function testClean()
    {
        $this->adapter->save('data', 'cacheId');
        $this->adapter->clean();
        $this->assertAttributeEmpty('cached', $this->adapter);
    }

    public function saveData()
    {
        return array(
            array(1),
            array('1'),
            array(array('1')),
            array(new \stdClass()),
            array(2,4),
        );
    }
}