<?php
namespace Sl\Common;

use Sl\Helper\Lists\ManagerInterface;

class HasListManagerTest extends \PHPUnit_Framework_TestCase
{
    /**
     *
     * @var HasListManager
     */
    protected $managable;

    /**
     *
     * @var ManagerInterface
     */
    protected $listsManager;
    
    public function setUp()
    {
        $this->managable = $this->getMockForTrait(HasListManager::class);
        $this->listsManager = $this->getMock(ManagerInterface::class);
    }
    
    public function testAssignListsManager()
    {
        $this->assertAttributeEmpty('listsManager', $this->managable);

        $this->managable->assignListsManager($this->listsManager);

        $this->assertAttributeEquals($this->listsManager, 'listsManager', $this->managable);
    }
    
    public function testFetchListsManager()
    {
        $this->setExpectedException(\Exception::class);
        $this->managable->fetchListsManager();
        
        $this->managable->assignListsManager($this->listsManager);
        $this->assertInstanceOf(ManagerInterface::class, $this->managable->fetchListsManager());
    }
}