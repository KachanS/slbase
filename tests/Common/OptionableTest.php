<?php
namespace Sl\Common;

class OptionableTest extends \PHPUnit_Framework_TestCase
{
    /**
     *
     * @var Optionable
     */
    protected $optionable;

    public function setUp()
    {
        $this->optionable = $this->getMockForTrait(Optionable::class);
    }

    public function testSetOption()
    {
        $this->optionable->setOption('a', 'b');
        $this->assertAttributeEquals(array('a' => 'b'), 'options', $this->optionable);
        $this->optionable->setOption('c', 'd');
        $this->assertAttributeEquals(array('a' => 'b', 'c' => 'd'), 'options', $this->optionable);
        $this->optionable->setOption('c', 'e');
        $this->assertAttributeEquals(array('a' => 'b', 'c' => 'e'), 'options', $this->optionable);
    }

    public function testHasOption()
    {
        $this->assertFalse($this->optionable->hasOption('a'));
        $this->optionable->setOption('a', 'b');
        $this->assertTrue($this->optionable->hasOption('a'));
    }

    public function testGetOptions()
    {
        $this->assertEquals(array(), $this->optionable->getOptions());
        $this->optionable->setOption('a', 'b')->setOption('c', 'd');
        $this->assertEquals(array('a' => 'b', 'c' => 'd'), $this->optionable->getOptions());
    }

    public function testGetOption()
    {
        $this->optionable->setOption('option', 'value');
        $this->assertEquals('value', $this->optionable->getOption('option'));
        $this->optionable->setOption('option', 'newValue');
        $this->assertEquals('newValue', $this->optionable->getOption('option'));
    }

    public function testAddOptions()
    {
        $this->optionable->addOptions(array('a' => 'b', 'c' => 'd'));
        $this->assertEquals(array('a' => 'b', 'c' => 'd'), $this->optionable->getOptions());
        $this->optionable->addOptions(array('c' => 'e', 'f' => 'g'));
        $this->assertEquals(array('a' => 'b', 'c' => 'e', 'f' => 'g'), $this->optionable->getOptions());
    }

    public function testSetOptions()
    {
        $this->optionable->setOptions(array('a' => 'b', 'c' => 'd'));
        $this->assertEquals(array('a' => 'b', 'c' => 'd'), $this->optionable->getOptions());
        $this->optionable->setOptions(array('c' => 'e', 'f' => 'g'));
        $this->assertEquals(array('c' => 'e', 'f' => 'g'), $this->optionable->getOptions());
    }

    public function testCleanOption()
    {
        $this->optionable->setOptions(array('a' => 'b', 'c' => 'd', 'e' => 'f'));
        $this->assertCount(3, $this->optionable->getOptions());
        $this->optionable->cleanOption('c');
        $this->assertEquals(array('a' => 'b', 'e' => 'f'), $this->optionable->getOptions());
    }

    public function testCleanOptions()
    {
        $this->optionable->setOptions(array('a' => 'b', 'c' => 'd'));
        $this->assertCount(2, $this->optionable->getOptions());
        $this->optionable->cleanOptions();
        $this->assertEmpty($this->optionable->getOptions());
    }
}