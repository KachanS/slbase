<?php
namespace Sl\Helper\Config\Writer;

use org\bovigo\vfs\vfsStream;
use org\bovigo\vfs\vfsStreamWrapper;
use org\bovigo\vfs\vfsStreamDirectory;

class ArrayWriterTest extends \PHPUnit_Framework_TestCase
{
    /**
     *
     * @var ArrayWriter
     */
    protected $writer;

    public function setUp()
    {
        $rootDirName = 'root';
        $this->initFilesystemWraper($rootDirName);
        
        $this->writer = new ArrayWriter();
    }

    public function testWrite()
    {
        $this->assertFileNotExists(vfsStream::url('root/config.php'));

        $this->writer->write(vfsStream::url('root/config.php'), array());
        $this->assertFileExists(vfsStream::url('root/config.php'));
        $this->assertEquals(<<<EMPTY_ARRAY
<?php return array(
);
EMPTY_ARRAY
, file_get_contents(vfsStream::url('root/config.php')));

        $this->writer->write(vfsStream::url('root/config.php'), array('a' => 'b', 'c' => array('w' => 'e')));
        $this->assertEquals(<<<SIMPLE_ARRAY
<?php return array(
    'a' => 'b',
    'c' => array(
        'w' => 'e',
    ),
);
SIMPLE_ARRAY
, file_get_contents(vfsStream::url('root/config.php')));
    }

    protected function initFilesystemWraper($rootDirName)
    {
        vfsStreamWrapper::register();
        vfsStreamWrapper::setRoot(new vfsStreamDirectory($rootDirName, 0777));
    }
}