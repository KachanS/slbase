<?php
namespace Sl\Helper;

class GenericTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @dataProvider mergeDataProvider
     *
     * @param array $a
     * @param array $b
     * @param array $result
     */
    public function testMerge($a, $b, $result)
    {
        $this->assertEquals($result, Generic::merge($a, $b));
    }

    /**
     * @dataProvider arrayValueDataProvider
     *
     * @param type $data
     * @param type $key
     * @param type $result
     * @param type $default
     * @param type $separator
     */
    public function testArrayValue($data, $key, $result, $default = null, $separator = '.')
    {
        $this->assertEquals($result, Generic::arrayValue($data, $key, $default, $separator));
    }

    public function mergeDataProvider()
    {
        return array(
            array(array(), array(), array()),
            array(array('a' => 'b'), array('b' => 'c'), array('a' => 'b', 'b' => 'c')),
            array(array('a' => 'b'), array('a' => 'c'), array('a' => 'c')),
            array(
                array('a' => array(1 => 'a', 2 => 'b')),
                array('a' => array(3 => 'c')),
                array('a' => array(1 => 'a', 'b', 'c'))
            ),
            array(array(), array('2' => '3'), array('2' => '3')),
            array(
                array('a' => array('b' => 'c')),
                array('a' => array('b' => 'd')),
                array('a' => array('b' => 'd')),
            ),
            array(
                array('a' => array('b' => array(0 => 'c'))),
                array('a' => array('b' => array(1 => 'd'))),
                array('a' => array('b' => array('c', 'd'))),
            ),
            array(
                array('a' => array('b' => array(0 => 'c'))),
                array('a' => array('b' => array(0 => 'd'))),
                array('a' => array('b' => array('d'))),
            ),
        );
    }

    public function arrayValueDataProvider()
    {
        return array(
            array(array('a' => 'b'), 'a', 'b'),
            array(array('a' => 'b'), 'c', null),
            array(array('a' => 'b'), 'c', 'd', 'd'),
            array(array('a' => array('b' => 'c')), 'c', null),
            array(array('a' => array('b' => 'c')), 'a', array('b' => 'c')),
            array(array('a' => array('b' => 'c')), 'a.b', 'c'),
            array(array('a' => array('b' => 'c')), 'a.b', 'c', 'd'),
            array(array('a' => array('b' => 'c')), 'a:b', null),
            array(array('a' => array('b' => 'c')), 'a.b', null, null, ';'),
            array(array('a' => array('b' => 'c')), 'a.b.c', null),
            array(array('a' => array('b' => 'c')), 'a.b.c', 'd', 'd'),
        );
    }
}