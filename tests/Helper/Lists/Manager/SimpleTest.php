<?php
namespace Sl\Helper\Lists\Manager;

class SimpleTest extends \PHPUnit_Framework_TestCase
{
    /**
     *
     * @var Simple
     */
    protected $manager;

    protected $listName = 'listName';
    protected $secListName = 'secListName';

    protected $validStatusName;
    protected $invalidStatusName;

    public function setUp()
    {
        $this->manager = new Simple();

        $this->validStatusName = Simple::STATUS_FINAL;
        $this->invalidStatusName = 'statusName';
    }

    public function testRegisterList()
    {
        $this->assertAttributeEmpty('lists', $this->manager);

        $this->manager->registerList($this->listName, array());
        $this->assertAttributeNotEmpty('lists', $this->manager);
        $this->assertAttributeCount(1, 'lists', $this->manager);

        $this->manager->registerList($this->listName, array(1 => 2, 3 => 4));
        $this->assertAttributeCount(1, 'lists', $this->manager);

        $this->manager->registerList($this->secListName, array(1 => 1, 2 => 3));
        $this->assertAttributeCount(2, 'lists', $this->manager);
    }

    public function testGetList()
    {
        $this->assertNull($this->manager->getList($this->listName));

        $this->manager->registerList($this->listName, array());
        $this->assertEquals(array(), $this->manager->getList($this->listName));
        $this->assertNull($this->manager->getList($this->secListName));

        $this->manager->registerList($this->listName, array(1 => 2, 3 => 4));
        $this->assertEquals(array(1 => 2, 3 => 4), $this->manager->getList($this->listName));
    }

    public function testGetListValue()
    {
        $key = 'someKey';
        $default = 'someDefaultValue';

        $this->assertNull($this->manager->getListValue($this->listName, $key));
        $this->assertEquals($default, $this->manager->getListValue($this->listName, $key, $default));

        $this->manager->registerList($this->listName, array());
        $this->assertNull($this->manager->getListValue($this->listName, $key));
        $this->assertEquals($default, $this->manager->getListValue($this->listName, $key, $default));

        $this->manager->registerList($this->listName, array(1 => 2, 3 => 4));
        $this->assertNull($this->manager->getListValue($this->listName, $key));
        $this->assertEquals($default, $this->manager->getListValue($this->listName, $key, $default));
        $this->assertEquals(2, $this->manager->getListValue($this->listName, 1));
        $this->assertEquals(2, $this->manager->getListValue($this->listName, 1, $default));
    }

    public function testRegisterListStatus()
    {
        $this->assertAttributeEmpty('statuses', $this->manager);

        $this->manager->registerListStatus($this->listName, $this->invalidStatusName, array());
        $this->assertAttributeEmpty('statuses', $this->manager);

        $this->manager->registerListStatus($this->listName, $this->validStatusName, array());
        $this->assertAttributeNotEmpty('statuses', $this->manager);
        $this->assertAttributeCount(1, 'statuses', $this->manager);

        $this->manager->registerListStatus($this->secListName, $this->validStatusName, array());
        $this->assertAttributeCount(2, 'statuses', $this->manager);

        $this->assertAttributeContainsOnly('array', 'statuses', $this->manager);
    }
    
    public function testRegisterListStatuses()
    {
        $this->manager->registerListStatuses(array());
        $this->assertAttributeEmpty('statuses', $this->manager);
        
        $this->manager->registerListStatuses(array('a' => 'b', 'c' => 'd'));
        $this->assertAttributeEmpty('statuses', $this->manager);

        $this->manager->registerListStatuses(array(
            $this->invalidStatusName => array(
                $this->listName => array(),
            ),
        ));
        $this->assertAttributeEmpty('statuses', $this->manager);

        $this->manager->registerListStatuses(array(
            $this->validStatusName => array(
                $this->listName => array(1, 2),
            ),
        ));
        $this->assertAttributeNotEmpty('statuses', $this->manager);
        $this->assertAttributeCount(1, 'statuses', $this->manager);
        $this->assertAttributeContainsOnly('array', 'statuses', $this->manager);

        $this->manager->registerListStatuses(array(
            $this->validStatusName => array(
                $this->secListName => array(2, 3),
                $this->listName => array(5, 6),
            ),
        ));

        $this->assertAttributeEquals(array(
            $this->listName => array(
                Simple::STATUS_FINAL => array(1, 2, 5, 6),
            ),
            $this->secListName => array(
                Simple::STATUS_FINAL => array(2, 3),
            ),
        ), 'statuses', $this->manager);
    }
    
    public function testGetListStatuses()
    {
        $this->assertNull($this->manager->getListStatuses($this->listName));

        $this->manager->registerListStatus($this->listName, $this->invalidStatusName, array());
        $this->assertNull($this->manager->getListStatuses($this->listName));

        $this->manager->registerListStatus($this->listName, $this->validStatusName, array());
        $this->assertEquals(array($this->validStatusName => array()), $this->manager->getListStatuses($this->listName));

        $this->manager->registerListStatus($this->secListName, $this->validStatusName, array(1, 3));
        $this->assertArrayHasKey(Simple::STATUS_FINAL, $this->manager->getListStatuses($this->secListName));
    }

    public function testCheckValueIsStatusValue()
    {
        $this->assertFalse($this->manager->checkValueIsStatusValue($this->listName, 1, $this->invalidStatusName));
        $this->assertFalse($this->manager->checkValueIsStatusValue($this->listName, 1, $this->validStatusName));

        $this->manager->registerListStatuses(array(
            $this->validStatusName => array(
                $this->secListName => array(1 => 2, 3 => 5),
                $this->listName => array(1 => 7, 8 => 12),
            ),
        ));
        $this->assertFalse($this->manager->checkValueIsStatusValue($this->secListName, 1, $this->validStatusName));
        $this->assertTrue($this->manager->checkValueIsStatusValue($this->secListName, 2, $this->validStatusName));
        $this->assertFalse($this->manager->checkValueIsStatusValue($this->listName, 5, $this->validStatusName));
        $this->assertTrue($this->manager->checkValueIsStatusValue($this->listName, 7, $this->validStatusName));
    }
}