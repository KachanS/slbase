<?php
namespace Sl\Helper\Model\Config\Reader;

use Sl\Model\ModelInterface;
use org\bovigo\vfs;

class SimpleTest extends \PHPUnit_Framework_TestCase
{
    /**
     *
     * @var Simple
     */
    protected $reader;

    /**
     *
     * @var ModelInterface
     */
    protected $validModel;

    /**
     *
     * @var ModelInterface
     */
    protected $invalidModel;

    protected $defaultModelName = 'basic';
    protected $defaultModuleName = 'main';

    protected $modelSectionName = 'model';
    protected $detailedSectionName = 'detailed';
    protected $formsConfigName = 'forms';
    
    protected $invalidSectionName = 'asd';

    public function setUp()
    {
        $rootDirName = 'root';
        $this->initFilesystemWraper($rootDirName);

        $this->reader = new Simple(array(
            $this->defaultModuleName => vfs\vfsStream::url($rootDirName),
        ));

        $this->validModel = $this->getMockForAbstractClass(ModelInterface::class);
        $this->validModel->expects($this->any())->method('findModuleName')->willReturn($this->defaultModuleName);
        $this->validModel->expects($this->any())->method('findModelName')->willReturn($this->defaultModelName);

        $this->invalidModel = $this->getMockForAbstractClass(ModelInterface::class);
        $this->invalidModel->expects($this->any())->method('findModuleName')->willReturn('');
        $this->invalidModel->expects($this->any())->method('findModelName')->willReturn('');
    }

    public function testRead()
    {
        $this->assertNull($this->reader->read($this->validModel, $this->invalidSectionName));
        $this->assertNull($this->reader->read($this->invalidModel, $this->detailedSectionName));
        $this->assertNull($this->reader->read($this->invalidModel, $this->invalidSectionName));

        $this->assertEquals(array('_default' => array('id', 'name', 'status')), $this->reader->read($this->validModel, $this->detailedSectionName));
        $this->assertCount(1, $this->reader->read($this->validModel, $this->modelSectionName));

        $module = $this->getMockForAbstractClass(ModelInterface::class);
        $module->expects($this->any())->method('findModuleName')->willReturn($this->defaultModuleName);
        $module->expects($this->any())->method('findModelName')->willReturn(null);
        
        $this->assertEquals(array('form' => array()), $this->reader->read($module, $this->formsConfigName));
    }

    protected function initFilesystemWraper($rootDirName)
    {
        vfs\vfsStreamWrapper::register();
        vfs\vfsStreamWrapper::setRoot(new vfs\vfsStreamDirectory($rootDirName));

        vfs\vfsStream::create(array(
            'externals' => array(
                $this->defaultModuleName => array(
                    Simple::ALLROLES_KEY => array(
                        $this->defaultModelName => array(
                            $this->detailedSectionName.'.php' => '<?php return array("_default" => array("id", "name", "status"),);',
                        ),
                    ),
                ),
            ),
            $this->defaultModelName => array(
                $this->modelSectionName.'.php' => '<?php return array("id" => array("label" => "Id", "type" => "text",),);'
            ),
            $this->formsConfigName.'.php' => '<?php return array("form" => array());',
        ), vfs\vfsStreamWrapper::getRoot());
    }
}