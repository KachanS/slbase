<?php
namespace Sl\Helper\Model;

class TreeTest extends \PHPUnit_Framework_TestCase
{
    /**
     *
     * @var \Sl\Model\Tree
     */
    protected $model;

    public function setUp()
    {
        $this->model = $this->getMockForAbstractClass(\Sl\Model\Tree::class);
    }

    public function testFillRootNode()
    {
        $this->assertNull($this->model->getLeftKey());
        $this->assertNull($this->model->getRightKey());
        $this->assertNull($this->model->getLevel());

        $this->model = Tree::fillRootNode($this->model);

        $this->assertEquals(1, $this->model->getLeftKey());
        $this->assertEquals(2, $this->model->getRightKey());
        $this->assertEquals(1, $this->model->getLevel());
    }

    public function testIsRoot()
    {
        $this->assertFalse(Tree::isRoot($this->model));

        $this->model = Tree::fillRootNode($this->model);
        $this->assertTrue(Tree::isRoot($this->model));

        $this->model->setLeftKey(2);
        $this->assertFalse(Tree::isRoot($this->model));

        $this->model = Tree::fillRootNode($this->model);
        $this->model->setLevel(2);
        $this->assertFalse(Tree::isRoot($this->model));

        $this->model = Tree::fillRootNode($this->model);
        $this->model->setRightKey(rand(2, 10));
        $this->assertTrue(Tree::isRoot($this->model));
    }
}