<?php
namespace Sl\Helper;

use Sl\Model\ModelInterface;

class ModelTest extends \PHPUnit_Framework_TestCase
{
    /**
     *
     * @var ModelInterface
     */
    protected $model;

    public function setUp()
    {
        $this->model = $this->getMockForAbstractClass(ModelInterface::class);

        $this->model    ->expects($this->any())
                        ->method('findModelName')
                        ->will($this->returnValue('name'));
        
        $this->model    ->expects($this->any())
                        ->method('findModuleName')
                        ->will($this->returnValue('moduleName'));
    }

    public function testGetAlias()
    {
        $this->assertEquals('moduleName.name', Model::getAlias($this->model));
    }

    /**
     *
     * @param type $property
     * @param type $method
     * @dataProvider setterDataProvider
     */
    public function testBuildSetter($property, $method)
    {
        $this->assertEquals($method, \Sl\Helper\Model::buildSetter($property));
    }

    /**
     *
     * @param type $property
     * @param type $method
     * @dataProvider getterDataProvider
     */
    public function testBuildGetter($property, $method)
    {
        $this->assertEquals($method, \Sl\Helper\Model::buildGetter($property));
    }

    /**
     *
     * @param type $property
     * @param type $method
     * @dataProvider randomMethodDataProvider
     */
    public function testBuildMethod($property, $method, $prefix)
    {
        $this->assertEquals($method, \Sl\Helper\Model::buildMethod($property, $prefix));
    }

    public function setterDataProvider()
    {
        return $this->providerDataBuilder('set');
    }

    public function getterDataProvider()
    {
        return $this->providerDataBuilder('get');
    }

    public function randomMethodDataProvider()
    {
        $prefix = substr(md5(microtime()), -10);
        return array_map(function($el) use($prefix) {
            $el[] = $prefix;
            return $el;
        }, $this->providerDataBuilder($prefix));
    }

    protected function providerDataBuilder($prefix)
    {
        return array_map(function($el) use($prefix) {
            $el[1] = $prefix.$el[1];
            return $el;
        }, array(
            array('test', 'Test'),
            array('test_A', 'TestA'),
            array('test_A', 'TestA'),
            array('test_a', 'TestA'),
            array('tEst_A', 'TestA'),
            array('test__a', 'TestA'),
            array('a_b_c', 'ABC'),
        ));
    }
}