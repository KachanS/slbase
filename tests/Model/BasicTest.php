<?php
namespace Sl\Model;

class BasicTest extends \PHPUnit_Framework_TestCase
{
    protected $model;

    protected $relationName = 'relationName';
    protected $secRelationName = 'secRelationName';

    protected $modelName = 'base';
    protected $moduleName = 'main';
    
    public function setUp()
    {
        $this->model = $this->getModelMock($this->modelName, $this->moduleName);
    }

    /**
     * 
     * @dataProvider simpleDataProvider
     */
    public function testSetGet($property, $value)
    {
        $setter = \Sl\Helper\Model::buildSetter($property);
        $getter = \Sl\Helper\Model::buildGetter($property);

        $this->model->$setter($value);
        $this->assertEquals($value, $this->model->$getter());
    }

    public function testAssignRelated()
    {
        $this->assertAttributeEmpty('related', $this->model);

        $this->model->assignRelated($this->relationName, array());
        $this->assertAttributeEquals(array(strtolower($this->relationName) => array()), 'related', $this->model);
        $this->assertAttributeCount(1, 'related', $this->model);

        $this->model->assignRelated($this->relationName, array($this->getModelMock('a', 'b'), 2));
        $this->assertEquals(array(0 => $this->getModelMock('a', 'b'), 2 => 2), $this->model->fetchRelated($this->relationName));
    }

    public function testFetchRelated()
    {   
        $this->assertEmpty($this->model->fetchRelated());
        $this->assertEmpty($this->model->fetchRelated($this->relationName));
        
        $this->model->assignRelated($this->relationName, array());
        $this->assertNotEmpty($this->model->fetchRelated());
        $this->assertEmpty($this->model->fetchRelated($this->relationName));
        
        $this->model->assignRelated($this->relationName, array(1, 2));
        $this->assertEquals(array(1 => 1, 2 => 2), $this->model->fetchRelated($this->relationName));

        $this->model->assignRelated($this->relationName, array(2, 3));
        $this->assertEquals(array(2 => 2, 3 => 3), $this->model->fetchRelated($this->relationName));

        $relatedA = $this->getModelMock('a', 'b');
        $relatedB = $this->getModelMock('b', 'a');

        $this->model->assignRelated($this->relationName, array($relatedA, $relatedB));
        $this->assertEquals(array($relatedA, $relatedB), $this->model->fetchRelated($this->relationName));

        $this->model->assignRelated($this->relationName, array(5 => $relatedA, 7 => $relatedB));
        $this->assertEquals(array(5 => $relatedA, 7 => $relatedB), $this->model->fetchRelated($this->relationName));

        $relatedA->setId(4);
        $relatedB->setId(7);
        
        $this->model->assignRelated($this->relationName, array($relatedA, $relatedB));
        $this->assertEquals(array(4 => $relatedA, 7 => $relatedB), $this->model->fetchRelated($this->relationName));

        $this->model->assignRelated($this->relationName, array(5 => $relatedA, 2 => $relatedB));
        $this->assertEquals(array(4 => $relatedA, 7 => $relatedB), $this->model->fetchRelated($this->relationName));
    }

    public function testFetchOneRelated()
    {
        $this->assertNull($this->model->fetchOneRelated($this->relationName));

        $this->model->assignRelated($this->relationName, array());
        $this->assertNull($this->model->fetchOneRelated($this->relationName));

        $this->model->assignRelated($this->relationName, array(1));
        $this->assertEquals(1, $this->model->fetchOneRelated($this->relationName));

        $this->model->assignRelated($this->relationName, array(5, 2));
        $this->assertEquals(5, $this->model->fetchOneRelated($this->relationName));

        $this->model->assignRelated($this->relationName, array($this->getModelMock('a', 'b'), $this->getModelMock('b', 'c')));

        $this->assertInstanceOf(ModelInterface::class, $this->model->fetchOneRelated($this->relationName));
        $this->assertEquals('a', $this->model->fetchOneRelated($this->relationName)->findModelName());
    }

    public function testClearRelated()
    {
        $this->model->assignRelated($this->relationName, array(1, 2));
        $this->model->assignRelated($this->secRelationName, array(2, 3));

        $this->model->clearRelated($this->relationName);
        $this->assertEmpty($this->model->fetchRelated($this->relationName));
        $this->assertNotEmpty($this->model->fetchRelated($this->secRelationName));
        $this->assertNotEmpty($this->model->fetchRelated());

        $this->model->clearRelated();
        $this->assertEmpty($this->model->fetchRelated());
    }

    public function testIssetRelated()
    {
        $this->assertFalse($this->model->issetRelated($this->relationName));
        $this->assertFalse($this->model->issetRelated($this->secRelationName));

        $this->model->assignRelated($this->relationName, array());
        $this->assertTrue($this->model->issetRelated($this->relationName));
        $this->assertFalse($this->model->issetRelated($this->secRelationName));

        $this->model->clearRelated($this->relationName);
        $this->assertFalse($this->model->issetRelated($this->relationName));
    }

    public function testExtract()
    {
        $this->assertEquals(array(
            'id' => '',
            'create' => '',
            'active' => '',
            'archived' => '',
            'extend' => ''
        ), $this->model->extract());

        $this->assertEquals(array(
            'id' => null,
            'create' => null,
        ), $this->model->extract(array(
            'id',
            'create',
        )));

        $this->model->fill(array(
            'id' => 123,
            'active' => 5,
            'extend' => '|asdasdqwe.qwe|',
            'modulerelation_first' => array(
                '1' => '1',
            ),
            'essentials' => array(
                'first' => array(
                    '2' => '2',
                ),
            ),
        ));

        $this->assertEquals(array(
            'id' => 123,
            'create' => '',
            'active' => 5,
            'archived' => '',
            'extend' => '|asdasdqwe.qwe|'
        ), $this->model->extract());

        $this->assertEquals(array(
            'id' => 123,
            'active' => 5,
            'extend' => '|asdasdqwe.qwe|',
            'someField' => null
        ), $this->model->extract(array(
            'id',
            'active',
            'extend',
            'someField',
        )));
    }
    
    public function testFill()
    {
        $this->model->fill(array(
            'id' => 12,
            'active' => 1,
            'whatever' => 'value',
            'modulerelation_test_names' => 'Test val',
            'modulerelation_test_' => 'asd',
            'modulerelation_test_btn' => 'qwe',
            'modulerelation_test' => 23,
            'modulerelation_test2_' => 'asd2',
            'modulerelation_test2_names' => 'qwe2',
            'modulerelation_rel' => ':newone',
            'essentials' => array(
                'testesse' => array(1, 2, 3),
                'testesse2' => array('', ),
            ),
        ));

        $this->assertEquals(12, $this->model->getId());
        $this->assertEquals(1, $this->model->getActive());
        $this->assertCount(2, $this->model->fetchRelated());
        $this->assertCount(1, $this->model->fetchRelated('test'));
        $this->assertCount(1, $this->model->fetchRelated('rel'));
        $this->assertEquals(23, $this->model->fetchOneRelated('test'));
        $this->assertCount(3, $this->model->fetchEssentials('testesse'));
        $this->assertTrue($this->model->issetEssentials('testesse2'));
        $this->assertEmpty($this->model->fetchEssentials('testesse2'));
    }

    public function testIsLoged()
    {
        $this->assertFalse($this->model->isLoged());

        $logedProperty = new \ReflectionProperty($this->model, 'loged');
        $logedProperty->setAccessible(true);
        $logedProperty->setValue($this->model, true);
        $logedProperty->setAccessible(false);

        $this->assertTrue($this->model->isLoged());
    }

    public function testFindControlField()
    {
        $this->assertNull($this->model->findControlField());

        $controlFieldProperty = new \ReflectionProperty($this->model, 'controlField');
        $controlFieldProperty->setAccessible(true);
        $controlFieldProperty->setValue($this->model, 'id');
        $controlFieldProperty->setAccessible(false);

        $this->assertEquals('id', $this->model->findControlField());
    }

    public function testFindControlStatus()
    {
        $this->assertNull($this->model->findControlStatus());

        $controlFieldProperty = new \ReflectionProperty($this->model, 'controlField');
        $controlFieldProperty->setAccessible(true);
        $controlFieldProperty->setValue($this->model, 'id');
        $controlFieldProperty->setAccessible(false);

        $this->model->setId((string) rand(0, 100));
        $this->assertEquals($this->model->getId(), $this->model->findControlStatus());
    }

    public function testFindFilledRelations()
    {
        $this->assertEmpty($this->model->findFilledRelations());

        $this->model->assignRelated($this->relationName, array());
        $this->model->assignRelated($this->secRelationName, array());
        $this->assertContains(strtolower($this->secRelationName), $this->model->findFilledRelations());
        $this->assertContains(strtolower($this->relationName), $this->model->findFilledRelations());
        $this->assertCount(2, $this->model->findFilledRelations());

        $this->model->clearRelated($this->relationName);
        $this->assertContains(strtolower($this->secRelationName), $this->model->findFilledRelations());
        $this->assertCount(1, $this->model->findFilledRelations());
    }

    public function testIsEmpty()
    {
        $this->assertTrue($this->model->isEmpty());

        $this->model->setId(1);
        $this->assertFalse($this->model->isEmpty());

        $this->model->setId(null);
        $this->assertTrue($this->model->isEmpty());

        $this->model->assignRelated($this->relationName, array());
        $this->assertTrue($this->model->isEmpty());

        $this->model->assignRelated($this->secRelationName, array(1));
        $this->assertFalse($this->model->isEmpty());
    }

    public function testListValue()
    {
        $this->model->assignListsManager(new \Sl\Helper\Lists\Manager\Simple());
        $this->assertNull($this->model->listValue('id'));
        $this->model->setId(1);
        $this->assertEquals($this->model->getId(), $this->model->listValue('id'));

        $modelReflection = new \ReflectionClass($this->model);
        $listsProperty = $modelReflection->getProperty('lists');
        $listsProperty->setAccessible(true);
        $listsProperty->setValue($this->model, array('id' => 'id_list'));
        $listsProperty->setAccessible(false);

        $this->model->fetchListsManager()->registerList('id_list', array(1 => 'new', 2 => 'closed'));

        $this->assertEquals('new', $this->model->listValue('id'));
        $this->assertEquals('closed', $this->model->listValue('id', 2));

    }

    public function testListsArray()
    {
        $this->assertEmpty($this->model->listsArray());
        $this->assertNull($this->model->listsArray('id'));
    }

    public function testAssignEssentials()
    {
        $this->assertAttributeEmpty('essentials', $this->model);

        $this->model->assignEssentials($this->relationName, array());
        $this->assertAttributeEquals(array(strtolower($this->relationName) => array()), 'essentials', $this->model);
        $this->assertAttributeCount(1, 'essentials', $this->model);

        $this->model->assignEssentials($this->relationName, array($this->getModelMock('a', 'b'), 2));
        $this->assertEquals(array(0 => $this->getModelMock('a', 'b'), 2 => 2), $this->model->fetchEssentials($this->relationName));

        $this->model->assignEssentials($this->secRelationName, array('', ''));
        $this->assertEmpty($this->model->fetchEssentials($this->secRelationName));
    }

    public function testFetchEssentials()
    {
        $this->assertEmpty($this->model->fetchEssentials($this->relationName));
        $this->assertEmpty($this->model->fetchEssentials($this->relationName, true));

        $this->model->assignEssentials($this->relationName, array());
        $this->assertEmpty($this->model->fetchEssentials($this->relationName));

        $this->model->assignEssentials($this->relationName, array(1, 2));
        $this->assertEquals(array(1 => 1, 2 => 2), $this->model->fetchEssentials($this->relationName));
        $this->assertEquals(array(1, 2), $this->model->fetchEssentials($this->relationName, true));

        $this->model->assignEssentials($this->relationName, array(2, 3));
        $this->assertEquals(array(2 => 2, 3 => 3), $this->model->fetchEssentials($this->relationName));
        $this->assertEquals(array(2, 3), $this->model->fetchEssentials($this->relationName, true));

        $relatedA = $this->getModelMock('a', 'b');
        $relatedB = $this->getModelMock('b', 'a');

        $this->model->assignEssentials($this->relationName, array($relatedA, $relatedB));
        $this->assertEquals(array($relatedA, $relatedB), $this->model->fetchEssentials($this->relationName));
        $this->assertEquals(array(0, 1), $this->model->fetchEssentials($this->relationName, true));

        $this->model->assignEssentials($this->relationName, array(5 => $relatedA, 7 => $relatedB));
        $this->assertEquals(array(5 => $relatedA, 7 => $relatedB), $this->model->fetchEssentials($this->relationName));
        $this->assertEquals(array(5, 7), $this->model->fetchEssentials($this->relationName, true));

        $relatedA->setId(4);
        $relatedB->setId(7);

        $this->model->assignEssentials($this->relationName, array($relatedA, $relatedB));
        $this->assertEquals(array(4 => $relatedA, 7 => $relatedB), $this->model->fetchEssentials($this->relationName));
        $this->assertEquals(array(4, 7), $this->model->fetchEssentials($this->relationName, true));

        $this->model->assignEssentials($this->relationName, array(5 => $relatedA, 2 => $relatedB));
        $this->assertEquals(array(4 => $relatedA, 7 => $relatedB), $this->model->fetchEssentials($this->relationName));
        $this->assertEquals(array(4, 7), $this->model->fetchEssentials($this->relationName, true));
    }

    public function testIssetEssentials()
    {
        $this->assertFalse($this->model->issetEssentials($this->relationName));
        $this->assertFalse($this->model->issetEssentials($this->secRelationName));

        $this->model->assignEssentials($this->relationName, array());
        $this->assertTrue($this->model->issetEssentials($this->relationName));
        $this->assertFalse($this->model->issetEssentials($this->secRelationName));
    }

    public function simpleDataProvider()
    {
        return array(
            array('id', rand(0, 10)),
            array('create', rand(0, 10)),
            array('active', rand(0, 10)),
            array('archived', rand(0, 10)),
            array('extend', rand(0, 10)),
        );
    }

    /**
     *
     * @param string $modelName
     * @param string $moduleName
     * @return Basic
     */
    protected function getModelMock($modelName, $moduleName)
    {
        $model = $this->getMockForAbstractClass(Basic::class);

        $model->expects($this->any())->method('findModelName')->will($this->returnValue($modelName));
        $model->expects($this->any())->method('findModuleName')->will($this->returnValue($moduleName));

        return $model;
    }
}